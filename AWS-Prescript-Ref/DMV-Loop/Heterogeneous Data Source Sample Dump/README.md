## MCM1-AI-Practicum

## Section 1: Student Details

Project Title:
Solution Architecture Recommendation System (synthesis of Cloud Native Hybrid arch utilizing System arch) employing ML Algorithmic approach.

Student ID(s):
23266990, 22267182

Student name(s):
Venkatraman Palani, 	Karthikeyan Pugazhandhi

Student email(s):
venkatraman.palani2@mail.dcu.ie, karthikeyan.pugazhandhi2@mail.dcu.ie 

Chosen major(s):
MSc Computing (AI Major), MSc Computing (DA Major)

Supervisor:
Michael Scriney
Date of Submission
23/11/2024


## Section 2: About your Practicum

## Please answer all questions below.  Please pay special attention to the word counts in all cases.


## What is the topic of your proposed practicum? (100 words)

The proposed practicum focuses on creating a cloud-based solution architecture recommendation system, leveraging machine learning (ML) algorithms. The central challenge is to recommend an optimal cloud architecture for given system architectures, using various ML models for analysis. This is a complex problem due to the diverse and dynamic nature of system architectures and cloud environments. The practicum will explore models like KMedoids, LDA, t-SNA, GANs, and Matrix Factorization, applied to different architectural paradigms – System Architecture, Cloud Architecture, and ML Architecture. The goal is to recommend the best cloud architecture based on system architecture inputs and ML-based analysis.

## Please provide details of the papers you have read on this topic (details of 5 papers expected).  – Reference List:

## Gao, M. et al. (2021) ‘Recommender systems based on generative adversarial networks: A problem-driven perspective’, Information Sciences, 546, pp. 1166–1185. doi: 10.1016/j.ins.2020.09.013.

The research paper discusses the application of Generative Adversarial Networks (GANs) in enhancing recommender systems (RS), which play a crucial role in filtering relevant options for users in their online activities. Despite their widespread use and effectiveness, recommender systems face two primary challenges: data noise and data sparsity. The paper outlines how GANs address these issues through adversarial perturbations and adversarial sampling-based training for data noise, and data augmentation for data sparsity. The study categorizes and reviews models that either generate user preferences by augmenting interactive information or synthesize user preferences by incorporating auxiliary information

## Nabli, H., Ben Djemaa, R. and Ben Amor, I. A. (2018) ‘Efficient cloud service discovery approach based on LDA topic modeling’, Journal of Systems and Software, 146, pp. 233–248. doi: 10.1016/j.jss.2018.09.069.


The research paper proposes a self-adaptive semantic-focused crawler based on Latent Dirichlet Allocation (LDA) for efficient discovery of cloud services. This crawler utilizes a Cloud Service Ontology (CSOnt), defining cloud service categories and containing concepts that allow the crawler to automatically collect and categorize cloud services. The crawler also prioritizes URLs to efficiently retrieve relevant cloud services and incorporates an ontology-learning function to enhance the Cloud Service Ontology, thereby maintaining the crawler's performance.



## Xie, Q. et al. (2016) ‘Asymmetric Correlation Regularized Matrix Factorization for Web Service Recommendation’, in 2016 IEEE International Conference on Web Services (ICWS), pp. 204–211. doi: 10.1109/ICWS.2016.34.

The research paper presents an asymmetric correlation regularized Matrix Factorization (MF) framework for web service recommendation, addressing the data sparsity problem in collaborative filtering techniques. It introduces asymmetric correlation, defined by an asymmetric matrix, to capture hidden correlations between user pairs or service pairs, thereby improving prediction accuracy. The paper proposes three MF models that integrate user and service asymmetric correlations, evaluated using a real-world Quality of Service (QoS) dataset. These models employ dependent relations and propagation of asymmetric correlations, demonstrating effectiveness in enhancing prediction accuracy for web service recommendation.

## Campbell, A., Caudle, K. and Hoover, R. C. (2019) ‘Examining Intermediate Data Reduction Algorithms for Use with T-SNE’, in Proceedings of the 2019 3rd International Conference on Compute and Data Analysis. New York, NY, USA: Association for Computing Machinery (ICCDA 2019), pp. 36–42. doi: 10.1145/3314545.3314549.


The paper explores the impact of using various intermediate data reduction algorithms, such as Principal Component Analysis, Independent Component Analysis, Linear Discriminant Analysis, Sammon Mapping, and Local Linear Embedding, prior to applying t-SNE (t-distributed Stochastic Neighbor Embedding) for data visualization. It emphasizes that each step in the visualization process is significant and that the choice of intermediate algorithm can lead to markedly different results, especially in tasks like clustering and multi-class data separation. The study concludes by underscoring the importance of carefully selecting dimensionality reduction techniques, considering the size and complexity of data, to extract more meaningful insights and pave the way for future advancements in data science.

## Zhou, Z. and Mu, L. (2016) ‘Representative Virtual Machine Templates: An optimized virtual machine templates management mechanism for an Cloud system based on K-medoids Clustering’, in 2016 35th Chinese Control Conference (CCC), pp. 5243–5248. doi: 10.1109/ChiCC.2016.7554171.


The paper introduces the concept of Representative Virtual Machine Templates (RVMTs) to address the challenge of limited storage for virtual machine (VM) templates in cloud data centers. RVMTs enable rapid deployment of a large scale of user VMs with various application purposes, using a limited number of representative templates. The paper proposes a K-medoids Clustering-based algorithm for finding RVMTs, aiming to minimize the average startup latency of all user VMs, and theoretically proves that this algorithm can achieve optimal results in the management and provisioning of VM templates.

## How does your proposal relate to existing work on this topic described in these papers?  (200 words)

Our proposal builds on the state of the art by integrating and comparing multiple ML approaches, such as KMedoids, LDA, and GANs, for recommending cloud architectures. Unlike the papers mentioned, which focus on singular aspects like optimization, personalization, or visualization, our project aims to provide a comprehensive analysis across various ML models. We plan to use a unique dataset encompassing a wide range of system and cloud architectures, differing from the more focused datasets used in the referenced papers. This broader dataset will allow for more generalizable and robust recommendations.

Moreover, our approach differs in that we intend to explore the effectiveness of ensemble methods and compare them against single-model approaches. Additionally, we aim to incorporate features like explainability and trustworthy AI into our recommendation system, aspects that are not extensively covered in the existing literature. This inclusion aligns with the growing demand for transparent and reliable AI solutions in cloud computing. Our project stands out by offering a comparative study of different ML models and their applicability in a practical, real-world scenario of cloud architecture recommendation, which is a novel approach in this field.

## What are the research questions that you will attempt to answer?  (200 words)

## What is the required feature set to sufficiently represent a system architecture and associated requirements?

The required feature set to represent a system architecture and its associated requirements is a complex task, as it depends heavily on the specific context and the objectives of the analysis. However, in the context of a recommendation system for cloud solution architectures, there are several key features that are generally important to consider:

## Cloud Native Architecture features:

Hardware Specifications (CPU type and speed, GPU requirements (if any), RAM size, storage capacity, and network bandwidth),
Software Stack (operating system, middleware, and application frameworks),
Scalability Requirements (vertically or horizontally load/ throughput influx)
Availability and Reliability Needs (FO, HA, DR),
Security and Compliance Requirements (like GDPR or HIPAA),
Network Architecture topologies (sNET, VPC, TGW, VPN, CDN and any specific latency or throughput requirements),
Performance Metrics (response time, throughput, and processing speed),
Cost Constraints (budgetary limits for cloud services & cost-effectiveness),
Interoperability Requirements (integration - Multi/ Hybrid cloud),
User and Traffic Patterns (distribution, peak usage times),
Data Management Needs (Data storage requirements, database types, data processing needs, and data transfer volumes),
Service Level Agreements (SLAs),


## System Architecture Features

Architecture Type (monolithic, microservices-based, serverless, or a hybrid),
Component Interactions (synchronous and asynchronous communication, message queues or service buses),
Dependency Management,
Load Balancing and Distribution,
Resource Utilization Patterns (CPU, memory, and storage utilization patterns)
Fault Tolerance and Redundancy (replication and failover strategies, high availability),
Scaling Strategies (vertical and horizontal scaling)
Maintenance and Update Procedures (downtime and rolling updates)


## Software Architecture Features

Programming Languages and Frameworks,
API Specifications (RESTful or GraphQL APIs, gRPC)
Data Flow and Processing Logic (data pipelines)
State Management (stateless or stateful)
Security Protocols (authentication, authorization, and encryption mechanisms)
Codebase Structure (modularity and libraries),
Test and Deployment Procedures (Automated CI/CD pipelines, DevOPs, MLOPs, AIOPs)
User Interface and Experience Considerations (hosting front-end components)


## Can recommender systems be used to recommend system architectures in response to provided requirements?

Collecting and analyzing the above mentioned features requires a thorough understanding of the system architecture and its operational context. The use of machine learning and deep learning algorithms in our recommendation system would involve using these features to train models that can predict the most suitable cloud architecture for a given set of system requirements.
Including system and software architecture features in the representation of a system for a cloud solution architecture recommendation system is essential. These features provide comprehensive insights into the functional and non-functional aspects of the system, influencing the suitability of different cloud architectures.
Collecting and analyzing these system and software architecture features allows for a more accurate and efficient recommendation of cloud architectures. They provide a comprehensive view of the system's operational and functional requirements, ensuring that the recommended cloud solution aligns with the overall system design and objectives.

## Can generative methods (GANs) be used to propose suitable architectures for previously unseen requirements?

Our proposal involves investigating a MLOps pipeline based cloud solution architecture recommendation system built on a suitable ML algorithmic framework.

By examining existing well-architected solutions 

Additional probing on ML models such as Partitioning Around Medoids (KMedoids - K-means clustering in Rust/ Python on Tensorflow or PyTorch) under progress.

The project is a feasibility study and comparison of analogous/ distinct Machine  Learning computability for various models.

Such as,
Topic modelling from LDA and representation using t-SNA + classification cluster based on topic/ commonality.

Then looking at GANs vs Matrix Factorization based recommendation approaches.

Alongside collaborative or content-based filtering (latent feature extraction) recommender system as safety net.

Comparison with GPT (General LLMs) vs Hyper Domain Specific custom LLMs or the above approaches.

To simply put, there a three domain distinct architectural paradigms of:

1) System Architecture (monolithic, microservice, hybrid, etc.,),
2) Cloud Architecture (Google Cloud, AWS, Agnostic cloud native Kubernetes, Apache Mesos, etc.,) &
3) ML architecture (KMedoids, K-means, LDA, t-SNA, GANs, Matrix Factorization, etc.,).

The project aim is to give recommendations for 2), using 1) as input & 3) as analysis model/ framework.
Feature Importance - What specific features are of most significance in predicting the compatibility and efficiency of a cloud architecture with a given system architecture? This question aims to identify key characteristics that drive effective cloud architecture recommendations. (preliminary topic modelling via LDA & collaborative or content-based filtering for latent feature extraction.)

Ensemble Methods vs Single Models - Can ensemble-based ML methods, combining various algorithms like KMedoids, LDA, and GANs, outperform single-model approaches in accuracy and reliability for recommending cloud architectures?
How will you explore these questions? (Please address the following points.  Note that three or four sentences on each will suffice.) 

## - What software and programming environment will you use?
The project will utilize Google Colab, GitLab, Python, Go/ Rust, D3js, SciPy/NumPy, Scikit-Learn, TensorFlow, PyTorch, and Pandas/Polars for data processing and model development.

## Cloud native environment: GCP & AWS. 

## Local development accelerator: TuringPi 2, RockChip - Turing RK1 NPU cluster, Nvidia Jetson Orin Nx hosted on: Apache Mesos DCOS, K8s, CRI-O, MLFlow & Hadoop.

## - What coding/development will you do? 
Development Work: We'll focus on feature engineering, analytics, and reporting. This includes developing algorithms for data preprocessing, model training, and result visualization.

## - What data will be used for your investigations?
Data Used:
Dataset: The dataset will comprise system and cloud architecture metrics and characteristics sourced from catalogue manifest of reference documentations - PDFs, HTML docs, code base templates - IaaC (Infrastructure as a Code), AaaC/ DaaC (Architecture/ Diagrams as a Code), component/ state descriptors, etc.,
Size: Expected to include thousands of records (documents - PDFs, HTML/ Markdown encoded pages, JSON Templates, Cloudformation code libraries, Terraform code repositories), encompassing various system and cloud architecture configurations. (~ 150 GB)
Availability: The dataset will be sourced from public repositories and cloud service providers, ensuring accessibility and relevance.

## - Is this data currently available, if not, where will it come from? 
Reference Architecture Examples and Best Practices

AWS Reference Architecture Diagrams

Reference architectures | Google Cloud
Cloud Architecture Guidance and Topologies
Browse Azure Architectures - Azure Architecture Center | Microsoft Learn
Azure Architecture Center | Microsoft Learn

Reference architecture diagram - Partner Center | Microsoft Learn

15 reference architectures for application developers | Enable Architect
Docs overview | hashicorp/aws | Terraform


## - What experiments do you expect to run? 
We plan to conduct experiments comparing different ML models (like KMedoids, LDA, GANs, K-Means, LLMs, etc.) in predicting the most suitable cloud architecture.
The output will include performance metrics like Cosine similarity, F1-score, precision, recall for classifiers, We'll also evaluate models using ROC/AUC curves.

In addition, as the project is based on recommender systems and generative approaches, user validation will be required.

## - What output do you expect to gather? 

 Results will be clustering classification, topic modelling and recommendation synthesis via various ML models/ approaches + Comparison metrics of equivalent systems.

## - How will the results be evaluated?
Evaluated through comparative analysis across methods from user validation, datasets, sampling techniques, and hyperparameter configurations. This includes comparing our results with current state-of-the-art models in equivalent capacity, alongside Domain SME cross-validation. 

 

