# Create a DataFrame with the extracted text using PyMuPDF
df_mupdf = pd.DataFrame(pdf_text_mupdf, columns=['PaperText'])

# Save the DataFrame to a new CSV file
csv_file_path_mupdf = "/mnt/data/extracted_text_mupdf.csv"
df_mupdf.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf


# Combine the text from the two PDFs into a single string for each, to store in a single row
text_part4 = ' '.join(pdf_text_mupdf)
text_part5 = ' '.join(new_pdf_text)

# Update the DataFrame by adding the new text in the second row
df_combined = pd.DataFrame([text_part4, text_part5], columns=['PaperText'])

# Save the updated DataFrame to the same CSV file
df_combined.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf


# Path for the newly uploaded PDF file (Part 6)
new_pdf_file_path_part6 = "/mnt/data/prescriptive-guidance_Part6.pdf"

# Open the new PDF file using PyMuPDF
new_pdf_document_part6 = fitz.open(new_pdf_file_path_part6)

# Extract text from each page of the new PDF
new_pdf_text_part6 = [page.get_text() for page in new_pdf_document_part6]

# Close the new PDF document
new_pdf_document_part6.close()

# Combine the text from Part 6 into a single string
text_part6 = ' '.join(new_pdf_text_part6)

# Append the new text to the existing DataFrame in a new row
df_combined.loc[2] = [text_part6]

# Save the updated DataFrame to the same CSV file
df_combined.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf

# Path for the newly uploaded PDF file (Part 7)
new_pdf_file_path_part7 = "/mnt/data/prescriptive-guidance_Part7.pdf"

# Open the new PDF file using PyMuPDF
new_pdf_document_part7 = fitz.open(new_pdf_file_path_part7)

# Extract text from each page of the new PDF
new_pdf_text_part7 = [page.get_text() for page in new_pdf_document_part7]

# Close the new PDF document
new_pdf_document_part7.close()

# Combine the text from Part 7 into a single string
text_part7 = ' '.join(new_pdf_text_part7)

# Append the new text to the existing DataFrame in a new row
df_combined.loc[3] = [text_part7]

# Save the updated DataFrame to the same CSV file
df_combined.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf

# Path for the newly uploaded PDF file (Part 8)
new_pdf_file_path_part8 = "/mnt/data/prescriptive-guidance_Part8.pdf"

# Open the new PDF file using PyMuPDF
new_pdf_document_part8 = fitz.open(new_pdf_file_path_part8)

# Extract text from each page of the new PDF
new_pdf_text_part8 = [page.get_text() for page in new_pdf_document_part8]

# Close the new PDF document
new_pdf_document_part8.close()

# Combine the text from Part 8 into a single string
text_part8 = ' '.join(new_pdf_text_part8)

# Append the new text to the existing DataFrame in a new row
df_combined.loc[4] = [text_part8]

# Save the updated DataFrame to the same CSV file
df_combined.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf

# Path for the newly uploaded PDF file (Part 9)
new_pdf_file_path_part9 = "/mnt/data/prescriptive-guidance_Part9.pdf"

# Open the new PDF file using PyMuPDF
new_pdf_document_part9 = fitz.open(new_pdf_file_path_part9)

# Extract text from each page of the new PDF
new_pdf_text_part9 = [page.get_text() for page in new_pdf_document_part9]

# Close the new PDF document
new_pdf_document_part9.close()

# Combine the text from Part 9 into a single string
text_part9 = ' '.join(new_pdf_text_part9)

# Append the new text to the existing DataFrame in a new row
df_combined.loc[5] = [text_part9]

# Save the updated DataFrame to the same CSV file
df_combined.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf


# Path for the newly uploaded PDF file (Part 10)
new_pdf_file_path_part10 = "/mnt/data/prescriptive-guidance_Part10.pdf"

# Open the new PDF file using PyMuPDF
new_pdf_document_part10 = fitz.open(new_pdf_file_path_part10)

# Extract text from each page of the new PDF
new_pdf_text_part10 = [page.get_text() for page in new_pdf_document_part10]

# Close the new PDF document
new_pdf_document_part10.close()

# Combine the text from Part 10 into a single string
text_part10 = ' '.join(new_pdf_text_part10)

# Append the new text to the existing DataFrame in a new row
df_combined.loc[6] = [text_part10]

# Save the updated DataFrame to the same CSV file
df_combined.to_csv(csv_file_path_mupdf, index=False)

csv_file_path_mupdf

