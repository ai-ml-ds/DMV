import pandas as pd

def clean_csv(input_csv_filename, output_csv_filename):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(input_csv_filename, header=None)
    # Drop rows where all elements are NaN
    df_cleaned = df.dropna(how='all')
    # Save the cleaned DataFrame to a new CSV file
    df_cleaned.to_csv(output_csv_filename, index=False, header=False)

# Example usage
input_csv = "output.csv"  # Replace with your input CSV file name
output_csv = "cleaned_output.csv"  # Replace with your desired output CSV file name

clean_csv(input_csv, output_csv)
import pandas as pd

def clean_csv(input_csv_filename, output_csv_filename):
    # Read the CSV file into a DataFrame
    df = pd.read_csv(input_csv_filename, header=None)
    # Drop rows where all elements are NaN
    df_cleaned = df.dropna(how='all')
    # Save the cleaned DataFrame to a new CSV file
    df_cleaned.to_csv(output_csv_filename, index=False, header=False)

# Example usage
input_csv = "output.csv"  # Replace with your input CSV file name
output_csv = "cleaned_output.csv"  # Replace with your desired output CSV file name

clean_csv(input_csv, output_csv)

