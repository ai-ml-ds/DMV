import PyPDF2
import csv
import os

# Define the filenames of the PDFs
pdf_filenames = [f"~/ai-ml-ds/dmvx0/DMV/AWS-Prescript-Ref/prescriptive-guidance_Part{i}.pdf" for i in range(4, 32)]

# Function to extract text from a single PDF file
def extract_text_from_pdf(pdf_file_path):
    with open(pdf_file_path, 'rb') as file:
        reader = PyPDF2.PdfFileReader(file)
        text = ""
        for page_num in range(reader.numPages):
            text += reader.getPage(page_num).extractText()
        return text

# Function to write the extracted texts to a CSV file
def write_texts_to_csv(pdf_filenames, csv_filename="output.csv"):
    with open(csv_filename, 'w', newline='', encoding='utf-8') as csv_file:
        csv_writer = csv.writer(csv_file)
        for pdf_file in pdf_filenames:
            if os.path.exists(pdf_file):
                text = extract_text_from_pdf(pdf_file)
                csv_writer.writerow([text])
            else:
                print(f"File not found: {pdf_file}")

# Execute the function
write_texts_to_csv(pdf_filenames)
