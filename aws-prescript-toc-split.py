import fitz  # PyMuPDF

def split_pdf_by_toc(pdf_path):
    # Open the original PDF
    doc = fitz.open(pdf_path)
    toc = doc.get_toc()

    # Check if the document has a TOC
    if not toc:
        return "The document has no table of contents."

    # Split the PDF based on the TOC
    split_files = []
    for i in range(len(toc)):
        start_page = toc[i][2] - 1  # TOC page numbers are 1-based
        end_page = toc[i + 1][2] - 1 if i + 1 < len(toc) else len(doc)

        # Check if the section has at least one page
        if start_page < end_page:
            section_doc = fitz.open()
            for page_num in range(start_page, end_page):
                section_doc.insert_pdf(doc, from_page=page_num, to_page=page_num)

            # Save the new PDF
            section_title = toc[i][1].replace('/', '_').replace('\\', '_')  # Replace characters not allowed in filenames
            section_filename = f"section_{i + 1}_{section_title}.pdf"
            section_doc.save(section_filename)
            section_doc.close()
            split_files.append(section_filename)

    # Close the original document
    doc.close()
    return split_files

# Path to the PDF file
pdf_path = '/mnt/c/Users/Venka/Documents/AWS-Prescript-Ref/prescriptive-guidance.pdf'

# Split the PDF and print the file paths of the split documents
split_files = split_pdf_by_toc(pdf_path)
for file in split_files:
    print(file)
